#include <bits/stdc++.h>
using namespace std;
#define lli long long int
vector <lli> adj[800001];
queue <lli> qu;
lli visited[800001];
lli visd[800001];
lli level[800001];
lli st[800001];
lli ft[800001];
lli lvl=1;
//lli flag=0;
lli tim=1;
lli timee[800001];
lli cnt;
vector <lli> arr[800001];


//SEGMENT TREE

lli insert(lli l,lli r,lli index)
{
	//	printf("insert\n");
	lli mid=(l+r)/2;
	//leaf node
	if(l!=r)
	{
		//		printf("l=%lld,r=%lld,ind=%lld\n",l,mid,2*index );
		insert(l,mid,2*index);
		//		printf("l=%lld,r=%lld,ind=%lld\n",mid+1,r,2*index+1 );		
		insert(mid+1,r,2*index+1);
		//		arr[index].push_back(timee[index]);
		//		arr[index]=arr[2*index]+arr[2*index+1];
		vector <lli> :: iterator it1;
		for(it1=arr[2*index].begin();it1<arr[2*index].end();it1++)
		{
			//			printf("push_back %lld -->> arr[%lld]\n",*it1,index );
			arr[index].push_back(*it1);
			//	arr[index].push_back(arr[2*index+1]);
		}

		vector <lli> :: iterator it2;
		for(it2=arr[2*index+1].begin();it2<arr[2*index+1].end();it2++)
		{
			//	arr[index].push_back(arr[2*index]);	
			//			printf("push_back %lld -->> arr[%lld]\n",*it2,index );			
			arr[index].push_back(*it2);
		}
		//		vector <lli> :: iterator tit;

		//		printf("sort\n");
		/*		for(tit=arr[index].begin();tit<arr[index].end();tit++)
				{
				printf("%lld ",*tit );
				}
				printf("\n");*/
		sort(arr[index].begin(),arr[index].end());		
	}
	else if (l==r)
	{
		arr[index].push_back(level[timee[l]]);		
		//		scanf("%lld",&arr[index]);
//		printf("push_back %lld -->> arr[%lld], l=%lld , timee[l]=%lld,\n",level[timee[l]],index,l,timee[l] );
		//		printf("arr[%lld]=%lld\n",index,arr[index] );
	}

	return 0;	
}


lli q(lli ql,lli qr,lli l,lli r,lli index,lli x)
{
//	printf("fn : q(ql=%lld,qr=%lld,l=%lld,r=%lld,index=%lld)\n", ql,qr,l,r,index);
	if(l>=ql && r<=qr)
	{
//		printf("in if 1\n");
//		vector <lli> :: iterator uit;
//		uit=upper_bound(arr[index].begin(),arr[index].end(),x);
//		vector <lli> :: iterator lit;
//		lit=lower_bound(arr[index].begin(),arr[index].end(),x);
//		lli low=(lit - arr[index].begin());
//		lli up=(uit - arr[index].end());
//		printf("%lld - %lld\n",up,low );
//		cnt+=up-low;
		pair<vector<lli> :: iterator ,vector<lli> :: iterator> it;
		it=equal_range(arr[index].begin(),arr[index].end(),x);
	/**	for(it1=arr[index].begin();it1<it;it1++)
			cnt++;*/
//			ans.push_back(*it1);
//		cout << it.first -arr[index].begin() << " a " << it.second - arr[index].begin() ;
//		lli add=count(arr[index].begin(),arr[index].end(),x);
		lli add = (it.second - arr[index].begin()) - (it.first -arr[index].begin());
//		lli add=*it.second - *it.first;
//		printf("add=%lld x=%lld\n",add,x );
		cnt+=add;
		return 0;
	}
//	if(ql<=l || qr>=r)
	if(qr<l || ql>r)
	{
//		printf("in if 2\n");
		return 0;
	}
//	printf("not if\n");
	lli mid=(l+r)/2;
//	printf("q(ql=%lld,qr=%lld,l=%lld,mid=%lld,2*index=%lld)+q(ql=%lld,qr=%lld,mid+1=%lld,r=%lld,2*index+1=%lld)\n",ql,qr,l,mid,2*index,ql,qr,mid+1,r,2*index+1);
	q(ql,qr,l,mid,2*index,x);
	q(ql,qr,mid+1,r,2*index+1,x);
	return 0;
}

lli bfs(lli s)
{

	while(qu.empty()!=1)
	{ 
		//		printf("%lld\n",qu.front() );
		//		printf("in while\n");
		//		printf("fv=%lld\n",visited[1] );
		//		printf("v=%lld\n",visited[qu.front()] );

		if(visited[qu.front()]!=1)
		{
			//			printf("if\n");

			visited[qu.front()]=1;
			lli tmp=qu.front();
			//***			printf("%lld ",qu.front() );
			qu.pop();

			vector <lli> :: iterator it;
			for(it=adj[tmp].begin();it<adj[tmp].end();it++)
			{
				if(visited[*it]!=1)
				{
				/*	if(flag==0)
					{
						lvl++;
						flag++;
					}*/
					qu.push(*it);
					level[*it]=level[tmp]+1;
//					printf("%lld -->>%lld\n",*it,lvl );
				}
			}

			bfs(qu.front());
			
		}
		else
			qu.pop();
		/*	printf("end \n");
			if (qu.empty()!=1)
			{
			printf("not empty\n");
			while(!qu.empty())
			{
			printf("%lld \n",qu.front() );
			qu.pop();
			}
			}
			else
			printf("empty\n");*/

		//		return 0;
	}

	return 0;

}


lli dfs(lli i)
{
	vector <lli> :: iterator tmp;
	//	tmp=adj[i];

	for(tmp=adj[i].begin();tmp < adj[i].end();tmp++)
	{	

		if(visd[*tmp]!=1)
		{
			visd[*tmp]=1;
			//cout << *tmp << endl;
			st[*tmp]=tim;
			timee[tim]=*tmp;
			tim++;
			dfs(*tmp);
//			printf("%lld \n",*tmp);
		}
	}
	ft[i]=tim;
	timee[tim]=i;
	tim++;

	return 0;
}

int main()
{
	lli i;
	lli n,nq;
	scanf("%lld %lld",&n,&nq);
	lli v,u;

	for(i=1;i<=n-1;i++)
	{
		scanf("%lld %lld",&v,&u);
		adj[v].push_back(u);
		adj[u].push_back(v);
	}
	qu.push(1);
	for(i=0;i<800001;i++)
		visited[i]=0;
	/*	for(i=0;i<800001;i++)
		printf("%lld ",visited[i]);
		printf("\n");*/
	level[1]=1;
	visd[1]=1;
	st[1]=tim;
	timee[tim]=1;
	tim++;
	bfs(1);
	dfs(1);
/*	printf("\n");
	for (i=1; i<=n;i++)
	{
		printf("%lld ",level[i] );
	}
	printf("\n");
	for (i=1; i<=n;i++)
	{
		printf("%lld ",st[i] );
	}
	printf("\n");
	for (i=1; i<=n;i++)
	{
		printf("%lld ",ft[i] );
	}
	printf("\n");*/

/*	for(i=1;i<=n;i++)
		arr[i].push_back(i);*/
	insert(1,2*n,1);

/*	vector <int> :: iterator it;
	
	for (i = 1; i <= 8*n; i++)
	{
		printf("%lld::\n",i );
		for (it = arr[i].begin(); it < arr[i].end() ; it++)
		{
			printf("%lld ",*it );
		}
		printf("\n");
	}*/

	for(i=1;i<=nq;i++)
	{
		lli vrtx,d;
		scanf("%lld %lld",&vrtx,&d);
		if(d==0)
			printf("1\n");
		else
		{
			q(st[vrtx],ft[vrtx],1,2*n,1,level[vrtx]+d);
			printf("%lld\n",cnt/2 );
		}	
		cnt=0;
	}

	return 0;
}
