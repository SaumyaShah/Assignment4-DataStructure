#include<bits/stdc++.h>
using namespace std;
#define lli long long int 
vector <lli> adj[100008];
lli rep[100008];
lli visited[100008];
lli w[100008];
lli b[100008];


lli wdfs(lli i)
{
	vector <lli> :: iterator tmp;
//	tmp=adj[i];

	for(tmp=adj[i].begin();tmp < adj[i].end();tmp++)
	{	
		// LEAF NODE--->> do return 0
		if(visited[*tmp]!=1)
		{
			visited[*tmp]=1;
//			cout << *tmp << " ";
			wdfs(*tmp);
//			printf("\n*tmp=%lld i=%lld\n",*tmp,i );
			if(w[*tmp]!=-1 && rep[i]==0)
			{
			//	w[i]+=w[*tmp]+1;
				w[i]+=w[*tmp];

			}
			else if(w[*tmp]!=-1 && rep[i]==1)
				w[i]+=w[*tmp];
		/*	lli j;
			for(j=1;j<=8;j++)
				printf("%lld ",w[j] );
			printf("\n");*/
		}

//		dp[i]=(dp[i]+dp[*tmp])%1000000007;
	}
	return 0;
}

lli bdfs(lli i)
{
	vector <lli> :: iterator tmp;
//	tmp=adj[i];

	for(tmp=adj[i].begin();tmp < adj[i].end();tmp++)
	{	
		// LEAF NODE--->> do return 0
		if(visited[*tmp]!=1)
		{
			visited[*tmp]=1;
//			cout << *tmp << " ";
			bdfs(*tmp);
//			printf("\n*tmp=%lld i=%lld\n",*tmp,i );
			if(b[*tmp]!=-1 && rep[i]==1)
			{
			//	w[i]+=w[*tmp]+1;
				b[i]+=b[*tmp];

			}
			else if(b[*tmp]!=-1 && rep[i]==0)
				b[i]+=b[*tmp];
		/*	lli j;
			for(j=1;j<=8;j++)
				printf("%lld ",b[j] );
			printf("\n");*/
		}

//		dp[i]=(dp[i]+dp[*tmp])%1000000007;
	}
	return 0;
}


int main()
{
	lli n;
	scanf("%lld",&n);

	lli i;
	for(i=1;i<=n;i++)
		scanf("%lld",&rep[i] );
	lli u,v;
	for(i=1;i<=n-1;i++)
	{
		scanf("%lld %lld",&u,&v);
		adj[u].push_back(v);
		adj[v].push_back(u);
	}

	for(i=1;i<=n;i++)
	{
		if(rep[i]==0)
			w[i]=1;
		else
			w[i]=-1;
	}

/*	for(i=1;i<=n;i++)
		printf("%lld ",b[i] );
	printf("\n");*/

/*	for(i=1;i<=n;i++)
		printf("%lld ",w[i] );
	printf("\n");*/

	for(i=1;i<=n;i++)
	{
		if(rep[i]==1)
			b[i]=1;
		else
			b[i]=-1;
	}

	visited[1]=1;
//	printf("1 ");
	wdfs(1);
//	printf("\n");

	for(i=0;i<100008;i++)
		visited[i]=0;

	visited[1]=1;
//	printf("1 ");
	bdfs(1);
//	printf("\n");

/*	for(i=1;i<=n;i++)
		printf("%lld ",w[i] );
	printf("\n");*/


/*	for(i=1;i<=n;i++)
		printf("%lld ",b[i] );
	printf("\n");*/

	lli maxb,maxw;
	maxw=*max_element(w,w+n);
	maxb=*max_element(b,b+n);

	printf("%lld\n",max(maxw,maxb) );

	return 0;
}